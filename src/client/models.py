from database import *
# Models for the tables in db
class User(db.Model):
    __tablename__='users'
    user_id=db.Column(db.Integer,primary_key=True)
    first_name=db.Column(db.String())
    last_name=db.Column(db.String())
    username=db.Column(db.String())
    password=db.Column(db.String())
    email=db.Column(db.String())

    def __init__(self, first_name, last_name, username, password, email):
        self.first_name = first_name
        self.last_name = last_name
        self.username = username
        self.password = password
        self.email=email