from flask import Flask, render_template, request
from flask_sqlalchemy import SQLAlchemy
#from models import *

from os import environ, path
from dotenv import load_dotenv

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI']='postgresql://postgres:root@localhost/mangiadb'
#basedir = path.abspath(path.dirname(__file__))
#load_dotenv(path.join(basedir, '.env'))

db=SQLAlchemy(app)
class User(db.Model):
    __tablename__='users'
    user_id=db.Column(db.Integer,primary_key=True)
    first_name=db.Column(db.String())
    last_name=db.Column(db.String())
    username=db.Column(db.String())
    password=db.Column(db.String())
    email=db.Column(db.String())

    def __init__(self, first_name, last_name, username, password, email):
        self.first_name = first_name
        self.last_name = last_name
        self.username = username
        self.password = password
        self.email=email

@app.route('/')
def index():
    return render_template('products.html')

@app.route('/login')
def login():
    return render_template('login.html')

@app.route('/register')
def register():
    return render_template('register.html')

@app.route('/submit_register', methods=['POST'])
def submit():
    first_name= request.form['first_name']
    last_name=request.form['last_name']
    username=request.form['username']
    password=request.form['password']
    email=request.form['email']

    user=User(first_name=first_name, last_name=last_name, username=username, password=password, email=email)
    db.session.add(user)
    db.session.commit()

    return render_template('success.html', data=first_name)

if __name__ == "__main__":
    app.run(debug=True)
